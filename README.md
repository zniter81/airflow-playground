# airflow-playground



## Getting started

start docker

```
docker-compose up
```

- download rule project from [Justin git] (https://gitlab.com/jpub/playground-odm-workspace)
- push/deploy Justin rule to ODM production.
- login to localhost:8080 [user:airflow password: airflow]
- run the follow docker command after u can login to the localhost:8080
```
docker-compose run airflow-cli connections import connection.json
```
- click the PLAY button("Trigger DAG"), for [zzz_create_table]
- insert SQL below SQL into postgres (see docker file for userid password etc)
```
insert into partners(execution_date, description, processed, jsondata)
select '2020-01-01', 'some description', null, '{"parentId": "S8111010G","childId": "T12312333G","numberOfDays":30,"salaryPerDay":4}'
```
- Postman this
    - URL: http://localhost:8080/api/v1/dags/zzz_process_a_claim/dagRuns
    - Basic Auth, airflow:airflow
    - Body
    ```
    {
        "conf": 
        {
            "dbId": get the ID from the insert statement above
        }
    }
    ```
- Play zzz_process_a_claim

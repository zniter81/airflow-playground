import json
from statistics import mode
from typing import Dict
from wsgiref.headers import tspecials
from airflow.decorators import task, dag
from airflow.sensors.sql import SqlSensor
from datetime import datetime
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.hooks.postgres_hook import PostgresHook
from types import SimpleNamespace

def _success_criteria(record):
    print(record)
    return record

def _failure_criteria(record):
    print(record)
    return True if not record else False

@dag(description='zzz_create_table', start_date=datetime(2021,1,1), schedule_interval=None, catchup=False)
def zzz_create_table():

    create_table = PostgresOperator(
        task_id="create_table",
        postgres_conn_id='postgres',
        sql="""
            CREATE TABLE IF NOT EXISTS partners (
                id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
                execution_date date NULL,
                description varchar(100) NULL,
                processed varchar(1) NULL,
                jsondata varchar(255) NULL
            );
          """,
    )

dag = zzz_create_table()
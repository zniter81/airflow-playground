import json
from statistics import mode
from typing import Dict
from wsgiref.headers import tspecials
from airflow.decorators import task, dag
from airflow.sensors.sql import SqlSensor
from datetime import datetime
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.hooks.postgres_hook import PostgresHook
from types import SimpleNamespace

def _success_criteria(record):
    print(record)
    return record

def _failure_criteria(record):
    print(record)
    return True if not record else False

@dag(description='zzz_process_a_claim', start_date=datetime(2021,1,1), schedule_interval=None, catchup=False)
def zzz_process_a_claim():

    def readDBFunction(**context):

        dbId = context["dag_run"].conf["dbId"]
        _readDB = PostgresHook(postgres_conn_id='postgres')
        connection = _readDB.get_conn()
        cursor = connection.cursor()
        cursor.execute('select id, processed, jsondata FROM partners where id = ' + str(dbId))
        sources = cursor.fetchone()
        return sources

    readDB = PythonOperator(task_id="readDB", python_callable=readDBFunction, provide_context=True)

    ######################################################################

    def formJson(ti, **context):
        record = ti.xcom_pull(
            task_ids='readDB',
            key='return_value'
        )

        json_object = json.loads(str(record[2]))
        print(json_object["parentId"])
        parentJson = '{"parentID": "' + json_object["parentId"] + '"}'
        ti.xcom_push("parentIdJson", parentJson)

        val = '{"__DecisionID__": "123123123", "mySubmissionDetails": ' + str(record[2]) + ' }'
        return val

    formODMJson = PythonOperator(
        task_id='formODMJson',
        python_callable=formJson,
        provide_context=True
    )

    ######################################################################

    def formJsonValidation(ti):
        jsonStr = ti.xcom_pull(
            task_ids='ODMResMaxClaim',
            key='return_value'
        )
        maxClaim_json_object = json.loads(str(jsonStr))

        fsrjsonStr = ti.xcom_pull(
            task_ids='mockfsr',
            key='return_value'
        )
        fsr_json_object = json.loads(str(fsrjsonStr))

        icajsonStr = ti.xcom_pull(
            task_ids='mockica',
            key='return_value'
        )
        ica_json_object = json.loads(str(icajsonStr))
        
        
        maxClaim_json_object["myMaxClaimDetails"]["icaChildId"] = ica_json_object["childID"]
        maxClaim_json_object["myMaxClaimDetails"]["fsrChildId"] = fsr_json_object["childID"]
        maxClaim_json_object["myMaxClaimDetails"]["fsrSpouseId"] = "not in use"

        print(maxClaim_json_object["myMaxClaimDetails"])
        val = '{"__DecisionID__": "67567567", "myValidationDetails": ' + json.dumps(maxClaim_json_object["myMaxClaimDetails"]) + ' }'
        return val

    formODMJsonForValidation = PythonOperator(
        task_id='formJsonValidation',
        python_callable=formJsonValidation,
    )

    ######################################################################

    calculateMaxClaimResponse = SimpleHttpOperator(
            task_id="ODMResMaxClaim",
            http_conn_id="ODMRes",
            method='POST',
            data="{{ ti.xcom_pull(task_ids='formODMJson', key='return_value') }}",
            endpoint='/deployClaimRule/1.0/flowClaimCalculation/1.0',
            headers={"Content-Type": "application/json", "Authorization": "Basic b2RtQWRtaW46b2RtQWRtaW4="},
            log_response=True,
            response_filter=lambda response: response.text
            # response_check=lambda response: response.json()['json']['priority'] == 5,
            # response_filter=lambda response: json.loads(response.text),
            # extra_options: Optional[Dict[str, Any]] = None,
            # log_response: bool = False,
            #auth_type: Type[AuthBase] = HTTPBasicAuth,
        )

    ######################################################################

    getICAData = SimpleHttpOperator(
            task_id="mockica",
            http_conn_id="mockica",
            method='POST',
            data="{{ ti.xcom_pull(task_ids='formODMJson', key='parentIdJson') }}",
            endpoint='/getICAChildInfo',
            headers={"Content-Type": "application/json"},
            log_response=True,
            response_filter=lambda response: response.text
            # response_check=lambda response: response.json()['json']['priority'] == 5,
            # response_filter=lambda response: json.loads(response.text),
            # extra_options: Optional[Dict[str, Any]] = None,
            # log_response: bool = False,
            #auth_type: Type[AuthBase] = HTTPBasicAuth,
        )

    ######################################################################

    getFSRData = SimpleHttpOperator(
            task_id="mockfsr",
            http_conn_id="mockfsr",
            method='POST',
            data="{{ ti.xcom_pull(task_ids='formODMJson', key='parentIdJson') }}",
            endpoint='/getFSRChildInfo',
            headers={"Content-Type": "application/json"},
            log_response=True,
            response_filter=lambda response: response.text
            # response_check=lambda response: response.json()['json']['priority'] == 5,
            # response_filter=lambda response: json.loads(response.text),
            # extra_options: Optional[Dict[str, Any]] = None,
            # log_response: bool = False,
            #auth_type: Type[AuthBase] = HTTPBasicAuth,
        )

    ######################################################################

    validateClaimResponse = SimpleHttpOperator(
            task_id="ODMResValidation",
            http_conn_id="ODMRes",
            method='POST',
            data="{{ ti.xcom_pull(task_ids='formJsonValidation', key='return_value') }}",
            endpoint='/deployClaimRule/1.0/flowValidation/1.0',
            headers={"Content-Type": "application/json", "Authorization": "Basic b2RtQWRtaW46b2RtQWRtaW4="},
            log_response=True,
            response_filter=lambda response: response.text
            # response_check=lambda response: response.json()['json']['priority'] == 5,
            # response_filter=lambda response: json.loads(response.text),
            # extra_options: Optional[Dict[str, Any]] = None,
            # log_response: bool = False,
            #auth_type: Type[AuthBase] = HTTPBasicAuth,
        )    

    ######################################################################

    removeMessage = PostgresOperator(
        task_id="removeMessageFromQueue",
        postgres_conn_id='postgres',
        sql="""
            DELETE FROM partners where id = {{ dag_run.conf["dbId"] }}
          """
        ##parameters={"dbid": "{{ ti.xcom_pull(task_ids='readDB', key='dbId') }}"},
    )

    readDB >> formODMJson >> calculateMaxClaimResponse >> [getICAData, getFSRData] >> formODMJsonForValidation >> validateClaimResponse >> removeMessage

dag = zzz_process_a_claim()